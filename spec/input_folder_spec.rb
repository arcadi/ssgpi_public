require_relative '../base/input_folder'
require_relative '../config'
require 'rspec'

describe InputFolder do
  let!(:list_of_filenames) { Dir.entries(INPUT_FOLDER_PATH) }

  context 'Input directory' do
    it 'should be the same as in config file' do
      InputFolder.pwd.should be_eql Dir.chdir(INPUT_FOLDER_PATH)
    end
  end

  context 'Scan input folder returns list of filenames' do
    it do
      InputFolder.scan_folder.should eq list_of_filenames
    end
  end

  context 'validate' do
    it 'file extenstion' do
      InputFolder.validate_file_extension(list_of_filenames).should be_true
    end

    it 'police zone' do
      InputFolder.validate_pz(list_of_filenames).should be_true
    end
  end

  context 'Define a bundle type' do
    context 'for OA' do
      # if (filename[0..1] == GO || FI || FS) && (fimename[3..6] == DMFA)
      let(:OA_file) { "GO.DMFA.103160.20101026.00001.R.1" }

      it do
        InputFolder.define_bundle_type(OA_file).should be_eql :OA
      end
    end

    context 'for WA' do
      # if (filename[0..1] == GO || FI || FS) && (fimename[3..6] == DMWA)
      let(:WA_file) { "GO.DMWA.103160.20131128.00001.R.1" }

      it do
        InputFolder.define_bundle_type(WA_file).should be_eql :WA
      end
    end

    context 'for FB' do
      # many cases - need to be clarified
      # if (filename[0..1] == GO || FS || FO || FO || FO || FO || FO || FO || L4 || TH) && (fimename[3..6] == ACRF || ACRF || ACRF || NOTI || DMPI || DMNO || PCAL || IDFL || STAS || PCAL)
      let(:FB_file) { "L4.STAS.0011579.20131128.000001.CSV" }

      it do
        InputFolder.define_bundle_type(FB_file).should be_eql :FB
      end
    end
  end
end
