require_relative '../../base/report/reporter'
require 'rspec'

describe Reporter do
  let!(:filename) { "tmpfile" }
  let!(:timestamp) { Time.now.strftime('%Y-%m-%d %H:%M:%S') }

  before do
    Reporter.stub(:get_timestamp).and_return(timestamp)
    Reporter.open(filename)
    Reporter.instance.reset!
  end

  describe '.write' do

    it 'should add data to storage' do
      Reporter.instance.storage.should be_a Hash
      Reporter.instance.current_log.should be_empty
      Reporter.write(:key1, :key2, :key3, {message: "my message 1"})
      Reporter.instance.storage.should_not be_empty
      Reporter.instance.storage[timestamp]['KEY1']['KEY2']['KEY3'][0]['message'].should == 'my message 1'
    end

    it 'should write data only from whitelist' do
      Reporter.instance.current_log.should be_empty
      Reporter.write(:key1, :key2, :key3, {mmm: "my message 1"})
      Reporter.instance.storage[timestamp]['KEY1']['KEY2']['KEY3'][0]['mmm'].should be_nil
    end

    it 'should add timestamp to record' do
      Reporter.instance.current_log.should be_empty
      Reporter.write(:key1, :key2, :key3, {mmm: "my message 1"})
      Reporter.instance.storage[timestamp]['KEY1']['KEY2']['KEY3'][0]['timestamp'].should_not be_nil
      Reporter.instance.storage[timestamp]['KEY1']['KEY2']['KEY3'][0]['timestamp'].should == timestamp
    end


  end

  describe '.flush' do

    it 'should write data to file' do
      Reporter.write(:key1, :key2, :key3, {message: "my message 1"})
      Reporter.write(:key1, :key2, :key3, {message: "my message 2"})
      Reporter.write(:key1, :key2, :key3, {message: "my message 3"})
      Reporter.flush

      File.exist?(filename).should be_true
      @yml = YAML.load(File.read(filename))
      @yml.should be_a(Hash)
      @yml[timestamp]['KEY1']['KEY2']['KEY3'][0]['message'].should == 'my message 1'
      @yml[timestamp]['KEY1']['KEY2']['KEY3'][0]['timestamp'].should include(Time.now.strftime("%Y-%m-%d %H:%M"))

    end

  end

end
