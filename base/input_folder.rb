class InputFolder < Dir
  ACCEPTED_TYPES = ['dmfa', 'dmwa', 'idfl', 'acrf', 'noti', 'dmpi', 'dmno', 'stas', 'pcal', 'xls']

  def initialize(path)
    @path = path
  end

  # Grab all files from input_folder
  def files
    begin
      Dir.entries(@path).map { |file_name| File.new("#{@path}/#{file_name}") unless file_name =~ /^\.\.?/ }.compact
    rescue Errno::ENOENT => e
      puts "#{e} Some files can not be found."
    end
  end

  # Grab all unprocessed files from input_folder
  def unprocessed_files
    self.files.select { |file| Time.parse(file.mtime.to_s) > Time.parse(timestamp.to_s) }
  end

  def xls_files
    unprocessed_files.select { |f| File.extname(f) == '.xls' }
  end

  def select_files_by_type(type)
    unprocessed_files.select { |f| file_classification(f) == type }
  end

  # Calling process for each file types
  def process
    begin
      ACCEPTED_TYPES.each { |type| define_files_processors(type.to_s).process }
      XlsProcessor.new(xls_files).process
    ensure
      self.timestamp = Timestamp.instance.value
    end
  end

  # Define process class for each file type
  def define_files_processors(type)
    Object.const_get("#{type.capitalize}Processor").new(select_files_by_type(type))
  end

  # Classify files by their type e.g. dmfa, dmwa, acrf etc
  def file_classification(file)
    File.basename(file).split('.')[1].to_s.downcase
  end

  # Timestamp is needed to define which files is not processed yet
  # So, we touch all processed files and update their modifed_time
  # After that we compare timestamp with file modifed_time
  # So, new files will be always have modifed_time > timestamp
  def timestamp
    # read timestamp from file
    @timestamp_file_name = File.join(@path, '.timestamp')
    @timestamp ||= begin
      if File.exist?(@timestamp_file_name) && !File.read(@timestamp_file_name).empty?
        Time.parse(File.read(@timestamp_file_name))
      else
        Time.new(0)
      end
    end
  end

  def timestamp=(new_timestamp)
    # write timestamp to file
    if timestamp != new_timestamp && !new_timestamp.to_s.empty?
      File.open(@timestamp_file_name, 'w') do |f|
        f.write(new_timestamp.to_s)
        f.close
      end
      @timestamp = new_timestamp
    end
  end
end
