class AcrfFile
  def initialize(file)
    @file = file
    @xml_file = XmlProcessor.new(file)
  end

  def search_for_linked_file_in_archive
    # File::FNM_CASEFOLD is for case incensitive search
    Dir.glob("#{ARCHIVE_FOLDER_PATH}/**/#{File.basename(linked_filename)}*", File::FNM_CASEFOLD)
  end

  def archive_year_with_quarter
    search_for_linked_file_in_archive.join.scan(/\d{4}Q\d/).first
  end

  def archive_jur_number
    search_for_linked_file_in_archive.join.scan(/\Jur\d{5}/).first
  end

  def linked_file_in_input_folder?
    @linked_file_in_input_folder = Dir.glob("#{INPUT_FOLDER_PATH}/#{File.basename(linked_filename)}*", File::FNM_CASEFOLD)
    !(@linked_file_in_input_folder.empty? || @linked_file_in_input_folder.nil?)
  end

  def input_year_with_quarter
    XmlProcessor.new(File.new(@linked_file_in_input_folder)).year_with_quarter
  end

  def input_jur_number
    XmlProcessor.new(File.new(@linked_file_in_input_folder)).jur_number
  end

  def destination_path
    if !search_for_linked_file_in_archive.empty?
      "#{ARCHIVE_FOLDER_PATH}/#{archive_year_with_quarter}/#{archive_jur_number}/OA"
    elsif linked_file_in_input_folder?
      "#{ARCHIVE_FOLDER_PATH}/#{input_year_with_quarter}/Jur#{input_jur_number}/OA"
    end
  end

  def linked_filename
    @xml_file.linked_filename
  end
end
