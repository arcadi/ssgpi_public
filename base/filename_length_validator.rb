class FilenameLengthValidator
  class << self
    #validation rules input file
    def valid?(file)
      validate_name_length(file) && validate_jur_length(file)
    end

    def validate_name_length(file)
      File.basename(file).split('.').size >= 6
    end

    def validate_jur_length(file)
      File.basename(file).split('.')[2].length == 6
    end
  end
end
