class DmwaFile
  def initialize(file)
    @file = file
    @xml_file = XmlProcessor.new(file)
  end

  def search_for_linked_file_in_archive
    # File::FNM_CASEFOLD is for case incensitive search
    Dir.glob("#{ARCHIVE_FOLDER_PATH}/**/FI.DMFA.#{File.basename(@file)[8..30]}*", File::FNM_CASEFOLD).join.scan(/\d{4}Q\d/).first
  end

  def search_for_linked_file_in_input_folder
    unless Dir.glob("#{INPUT_FOLDER_PATH}/FI.DMFA.#{File.basename(@file)[8..30]}*", File::FNM_CASEFOLD).empty?
      # File::FNM_CASEFOLD is for case incensitive search
      XmlProcessor.new(File.new(Dir.glob("#{INPUT_FOLDER_PATH}/FI.DMFA.#{File.basename(@file)[8..30]}*", File::FNM_CASEFOLD).join)).year_with_quarter
    end
  end

  def version
    @xml_file.version
  end

  def jur_number
    @xml_file.jur_number
  end

  def destination_path
    if !(search_for_linked_file_in_archive.nil? || search_for_linked_file_in_archive.empty?)
      "#{ARCHIVE_FOLDER_PATH}/#{search_for_linked_file_in_archive}/Jur#{jur_number}/WA/#{version}"
    elsif !(search_for_linked_file_in_input_folder.nil? || search_for_linked_file_in_input_folder.empty?)
      "#{ARCHIVE_FOLDER_PATH}/#{search_for_linked_file_in_input_folder}/Jur#{jur_number}/WA/#{version}"
    end
  end
end
