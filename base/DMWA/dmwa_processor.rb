class DmwaProcessor
  def initialize(files)
    @files = validate!(files)
  end

  # validate input files
  def validate!(files)
    files.select do |f|
      validate_type_match(f) && FilenameLengthValidator.valid?(f)
    end
  end

  # validation rules input file
  def validate_type_match(f)
    ['fi', 'go', 'fs'].include? File.basename(f).split('.')[0].downcase
  end

  # process input files
  def process
    @files.each do |f|
      if select_linked_files(f).size < 3
        select_linked_files(f).each do |file|
          Reject.new(file, 'FI, FS or GO file is missing')
        end
      else
        perform_by_classification_data(f) if File.basename(f).split('.')[0].downcase == 'fi'
      end
    end
  end

  def perform_by_classification_data(f)
    if has_xml_header?(f)
      move_to_archive(f, DmwaFile.new(f).destination_path)
    else
      Reject.new(f, "has non xml content")
    end
  end

  def move_to_archive(f, path)
    select_linked_files(f).each do |file|
      if path.nil?
        Reject.new(file, "there is no linked FI.DMFA file found in ARCHIVE and INPUT FOLDER")
      else
        FileUtils.mkdir_p(path)
        FileUtils.cp(file.path, path)
        # Write to file is a FileUtils.touch replacement which not working on windows
        # It's needed for updating file.mtime
        File.open(file, 'a').print ' '
        set_timestamp(file)

        puts "#{File.basename(file)} was copied to #{path}".light_yellow
      end
    end
  end

  def select_linked_files(f)
    @files.select do |file|
      File.basename(file)[3..30] == File.basename(f)[3..30]
    end
  end

  protected

  def set_timestamp(f)
    Timestamp.instance.value = f.mtime
  end

  # helper methods
  def has_xml_header?(f)
    File.foreach(f).first =~ /^\<\?xml/ || Nokogiri::XML(f).errors.empty?
  end
end
