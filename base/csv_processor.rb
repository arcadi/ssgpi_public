class CsvProcessor
  def initialize(kbo)
    @kbo_number = kbo
  end

  def find_jur_number
    jur_number = ''

    begin
      file = File.open(WERKGEVERS_PATH, "r:ISO-8859-1")
      CSV.parse(file, :headers => true, :col_sep => ';').find do |row|
        jur_number = row['Jur'] if row['KBO'] == @kbo_number
      end
      jur_number.to_s
    rescue => e
      puts "WERKGEVERS Parse ERROR: #{e}".red
    end
  end
end
