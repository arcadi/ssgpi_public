class StasProcessor
  def initialize(files)
    @files = validate!(files)
  end

  # validate input files
  def validate!(files)
    files.select do |f|
      valid_extension?(f) && valid_type_match?(f) && valid_name_length?(f)
    end
  end

  def valid_extension?(f)
    File.extname(f).downcase == '.csv'
  end

  # validation rules input file
  def valid_type_match?(f)
    ['l4', 'go'].include? File.basename(f).split('.')[0].downcase
  end

  def valid_name_length?(f)
    File.basename(f).split('.').size == 6
  end

  # process input files
  def process
    @files.each do |f|
      if select_linked_files(f).size < 2
        select_linked_files(f).each do |file|
          Reject.new(file, 'L4 or GO file is missing')
        end
      else
        perform_by_classification_data(f) if File.basename(f).split('.')[0].downcase == 'l4'
      end
    end
  end

  def perform_by_classification_data(f)
    move_to_archive(f)
  end

  def move_to_archive(f)
    select_linked_files(f).each do |file|
      path = StasFile.new(f).destination_path
      if path.nil?
        Reject.new(file, "couldn't parse L4.STAS file. Please check the file")
      else
        FileUtils.mkdir_p(path)
        FileUtils.cp(file.path, path)
        # Write to file is a FileUtils.touch replacement which not working on windows
        # It's needed for updating file.mtime
        File.open(file, 'a').print ' '
        set_timestamp(file)

        puts "#{File.basename(file)} was copied to #{path}".light_white
      end
    end
  end

  def select_linked_files(f)
    @files.select do |file|
      File.basename(file)[3..30] == File.basename(f)[3..30]
    end
  end

  protected

  def set_timestamp(f)
    Timestamp.instance.value = f.mtime
  end
end
