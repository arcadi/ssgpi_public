class StasFile
  def initialize(file)
    @file = File.open(file, "r:ISO-8859-1")
    read
  end

  def read
    CSV.parse(@file, :headers => true, :col_sep => ';').find do |row|
      @year = row[2][0..3]
      @quarter = row[2][-1]
      @jur_number = row[1]
      @version = row[3]
    end
  end

  def version_path
    if @version == '1'
      'OA'
    else
      "WA/T#{'%03d' % @version}" if !@version.nil?
    end
  end

  def destination_path
    if !version_path.nil?
      "#{ARCHIVE_FOLDER_PATH}/#{@year}Q#{@quarter}/Jur#{@jur_number}/#{version_path}"
    end
  end
end
