class ReferenceOriginFile
  def initialize(file)
    @file = file
    @xml_file = XmlProcessor.new(file)
  end

  def reference_origin_number
    @xml_file.reference_origin_number
  end

  def jur_number
    reference_origin_number[2..6]
  end

  def year
    reference_origin_number[7..10]
  end

  def quarter
    reference_origin_number[11]
  end

  def version
    reference_origin_number[-4..-1]
  end

  def version_path
    if version == 'T001'
      'OA'
    else
      "WA/#{version}"
    end
  end

  def destination_path
    if !reference_origin_number.nil?
      "#{ARCHIVE_FOLDER_PATH}/#{year}Q#{quarter}/Jur#{jur_number}/#{version_path}"
    end
  end
end
