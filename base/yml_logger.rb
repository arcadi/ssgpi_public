class YmlLogger
  def initialize(path, message, date)
    @path = path
    @message = message
    @date = date
    write_to_log
  end

  def write_to_log
    File.open("#{LOG_PATH}/log.yml", "a") do |f|
      f.write[@path] = @message + @date
    end
  end
end
