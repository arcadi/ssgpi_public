class IdflFile
  def initialize(file)
    @xml_file = XmlProcessor.new(file)
  end

  def kbo_number
    @xml_file.kbo_number
  end

  def read_jur_number
    if !(CsvProcessor.new(kbo_number).find_jur_number.nil? || CsvProcessor.new(kbo_number).find_jur_number.empty?)
      CsvProcessor.new(kbo_number).find_jur_number
    end
  end

  def destination_path
    "#{INDIVIDUAL_INFO_PATH}/Jur#{read_jur_number}" unless read_jur_number.nil?
  end
end
