class XmlProcessor
  def initialize(file)
    @xml_file = Nokogiri::XML(file)
  end

  def jur_number
    @xml_file.css("ReferenceNbr").text[2..6]
  end

  def quarter
    @xml_file.css("Quarter").text[-1]
  end

  def year
    @xml_file.css("Quarter").text[0..3]
  end

  def kbo_number
    @xml_file.css("IdfluxInformation CompanyID").text
  end

  def version
    @xml_file.css("ReferenceNbr").text[-4..-1]
  end

  def year_with_quarter
    "#{year}Q#{quarter}"
  end

  def linked_filename
    @xml_file.css("FileReference FileName").text
  end

  def reference_origin_number
    if !@xml_file.css('Reference ReferenceNbr').text.match(/\d{12}T\d{3}/).nil?
      @xml_file.css('Reference ReferenceNbr').text
    end
  end
end
