class Reject
  def initialize(file, message)
    @file = file
    @message = message
    reject
  end

  def reject
    FileUtils.mkdir_p(REJECTED_PATH)
    FileUtils.cp(@file.path, REJECTED_PATH)
    create_timestamp

    puts "File #{File.basename(@file)} was rejected because #{@message}".white.on_red
  end

  def create_timestamp
    Timestamp.instance.value = @file.mtime
  end
end
