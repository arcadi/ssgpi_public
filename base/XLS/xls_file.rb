class XlsFile
  def initialize(file)
    @file = file
  end

  def year
    File.basename(@file).split('.')[3][0..3]
  end

  def quarter
    # 3 is part of name eg 20103
    # -1 is a last char from this part
    File.basename(@file).split('.')[3][-1]
  end

  def jur_number
    File.basename(@file).split('.')[2].sub!(/^00/, "")
  end

  def version
    File.basename(@file).split('.')[4]
  end

  def version_path
    if version == 'T001'
      'OA'
    else
      "WA/#{version}"
    end
  end

  def destination_path
    "#{ARCHIVE_FOLDER_PATH}/#{year}Q#{quarter}/Jur#{jur_number}/#{version_path}"
  end
end
