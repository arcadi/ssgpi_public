class XlsProcessor
  def initialize(files)
    @files = validate!(files)
  end

  # validate input files
  def validate!(files)
    files.select do |f|
      validate_name_allowability(f) && validate_name_length(f)
    end
  end

  # validation rules input file
  def validate_name_allowability(f)
    File.basename(f).split('.')[0].downcase == 'th' && File.basename(f).split('.')[1].downcase == 'pcal'
  end

  def validate_name_length(f)
    File.basename(f).split('.').size == 7
  end

  # process input files
  def process
    @files.each do |f|
      move_to_archive(f, XlsFile.new(f).destination_path)
    end
  end

  def move_to_archive(f, path)
    FileUtils.mkdir_p(path)
    FileUtils.cp(f.path, path)
    # Write to file is a FileUtils.touch replacement which not working on windows
    # It's needed for updating file.mtime
    File.write(f, ' ', File.size(f), :mode => 'a')
    set_timestamp(f)

    puts "#{File.basename(f)} was copied to #{path}".light_green
  end

  protected

  def set_timestamp(f)
    Timestamp.instance.value = f.mtime
  end
end
