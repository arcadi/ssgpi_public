require 'singleton'
require 'yaml'
class Reporter
  include Singleton

  WHITE_LIST = %w[timestamp message reason filename]
  USE_WHITE_LIST = true

  attr_accessor :current_log, :storage, :filename, :timestamp

  def open(filename)
    @filename = filename
    if File.exist?(filename)
      @storage = YAML.load(File.read(filename))
    end
    @storage ||= {}
  end

  def reset!
    @storage[timestamp] = nil if timestamp
    @timestamp = get_timestamp
    @storage[timestamp] = {}
    @current_log = @storage[timestamp]
  end

  def write(*keys, params)
    raise 'Please before open file' unless filename
    push_to_array(tree_path(*keys), params)
  end

  def flush
    File.open(filename, 'w') { |f| f.write storage.to_yaml }
  end

  private

  def push_to_array(array, params)
    hash = add_timestamp(sanitize_hash(params))
    array.push(hash)
  end

  def tree_path(*keys)
    hash = current_log
    keys[0..-2].each do |key|
      hash[tree_key(key)] = {} if  hash[tree_key(key)].nil?
      hash = hash[tree_key(key)]
    end
    hash = (hash[tree_key(keys.last)] ||= [])
    hash
  end

  def tree_key(key)
    key.to_s.upcase
  end

  def value_key(key)
    key.to_s
  end

  def sanitize_hash(params)
    hash = {}
    params.each do |key, value|
      next if use_whitelist? && !WHITE_LIST.include?(value_key(key))
      hash[value_key(key)] = value
    end
    hash
  end

  def get_timestamp(time=Time.now)
    time.strftime('%Y-%m-%d %H:%M:%S')
  end

  def add_timestamp(hash)
    hash = {value_key("timestamp") => get_timestamp}.merge(hash) unless hash[value_key("timestamp")]
    hash
  end

  def use_whitelist?
    USE_WHITE_LIST
  end

  private

  class << self

    def open(*args)
      instance.open(*args)
    end


    def write(*args)
      instance.write(*args)
    end

    def flush
      instance.flush
    end

  end


end