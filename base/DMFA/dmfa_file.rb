class DmfaFile
  def initialize(file)
    @xml_file = XmlProcessor.new(file)
  end

  def destination_path
    "#{ARCHIVE_FOLDER_PATH}/#{@xml_file.year_with_quarter}/Jur#{@xml_file.jur_number}/OA"
  end
end
