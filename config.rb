BASE_PATH = './base'
DEFAULT_DIR = './route_social_declarations'
INPUT_FOLDER_PATH = "#{DEFAULT_DIR}/input_folder"
ARCHIVE_FOLDER_PATH = "#{DEFAULT_DIR}/archive"
REJECTED_PATH = "#{ARCHIVE_FOLDER_PATH}/rejected"
INDIVIDUAL_INFO_PATH = "#{ARCHIVE_FOLDER_PATH}/individual_info"
WERKGEVERS_PATH = './Werkgevers.csv'
LOG_PATH = './log'
