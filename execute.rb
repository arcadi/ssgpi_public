#!/bin/ruby

# Require all needed libs
begin
  require 'colorize'
  require 'time'
  require 'fileutils'
  require 'nokogiri'
  require 'csv'
  require 'yaml'
rescue LoadError => e
  puts "#{e}. Installing..."
  # Installing missed gems
  system('gem install colorize')
  system('gem install fileutils')
  system('gem install nokogiri')
end


# include all needed files
require_relative 'config'
require_relative "#{BASE_PATH}/reference_origin_file"
require_relative "#{BASE_PATH}/reference_origin_processor"
Dir["#{BASE_PATH}/**/*.rb"].each { |file| require_relative file }

input_folder = InputFolder.new(INPUT_FOLDER_PATH)
puts "Scanning #{INPUT_FOLDER_PATH}".green

begin

  Reporter.open(LOG_PATH)

  while input_folder.unprocessed_files.size > 0 do
    input_folder.process
  end

ensure
  Reporter.flush
end

puts 'Scanning was successfully finished'.black.on_green
